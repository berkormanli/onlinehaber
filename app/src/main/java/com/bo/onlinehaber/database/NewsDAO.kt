package com.bo.onlinehaber.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.bo.onlinehaber.Articles
import com.bo.onlinehaber.NewsDBStructure

@Dao
interface NewsDAO {

    @Insert
    suspend fun insertAll(vararg news: Articles):List<Long>

    //Insert -> Room, insert into
    //suspend -> coroutine scope
    //vararg -> birden fazla ve istediğimiz sayıda
    //list<long> -> long, idler

    @Query("SELECT * FROM article")
    suspend fun getAllNews() : List<Articles>

    @Query("SELECT * FROM article WHERE url = :url")
    suspend fun getNews(url:String) : Articles?

    @Query("DELETE FROM article")
    suspend fun deleteAllNews()


    @Insert
    suspend fun insertSaveNews(vararg news:NewsDBStructure)

    @Query("SELECT * FROM news WHERE isSaved =:isSaved")
    suspend fun getAllSaveNews(isSaved: Int): List<NewsDBStructure>

    @Query("DELETE FROM news WHERE url=:url")
    suspend fun deleteSaveNews(url:String)

    @Query("SELECT * FROM news WHERE url=:url")
    suspend fun getSaveNews(url: String): NewsDBStructure?

    @Query("UPDATE news SET isSaved =:isSaved WHERE url=:url")
    suspend fun updateSaved(isSaved:Int,url: String)

}