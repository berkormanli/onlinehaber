package com.bo.onlinehaber.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.bo.onlinehaber.Articles
import com.bo.onlinehaber.NewsDBStructure

@Database(entities = [Articles::class, NewsDBStructure::class],version = 1)

abstract class NewsDB : RoomDatabase() {

    abstract fun newsDao() : NewsDAO

    companion object{

        @Volatile private var instance : NewsDB? = null

        private val lock = Any()
        operator fun invoke(context: Context) = instance ?: synchronized(lock){
            instance ?: createDatabase(context).also {
                instance = it
            }
        }

        private fun createDatabase(context: Context) = Room.databaseBuilder(context.applicationContext,NewsDB::class.java,"newsdatabase").build()
    }
}