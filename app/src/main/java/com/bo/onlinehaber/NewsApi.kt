package com.bo.onlinehaber

import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

class NewsApi {
    val BASE_URL = "http://newsapi.org/v2/"
    val API_KEY = "449d1a18c53a42fbab118c8188e876e3"

    private val api = RetrofitClient().getClient(BASE_URL).create(ApiInterface::class.java)

    fun getSearchData(title:String, content:String,
                      to:String, from:String,
                      language:String, sortBy:String): Single<NewsItem> {
        return api.getEverything(title,content,from,to,language,sortBy,API_KEY)
    }
    fun getData() : Single<NewsItem> {
        return api.getNews()
    }

    fun getBottomDataCountry(country:String): Single<NewsItem> {
            return api.countrySearch(country,API_KEY)
    }

    fun getBottomDataLanguage(language:String): Single<NewsItem> {
        return api.languageSearch(language,API_KEY)
    }

}

class RetrofitClient {

    fun getClient(baseUrl:String):Retrofit{

        var interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client: OkHttpClient = OkHttpClient.Builder().addInterceptor(interceptor).build()

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    }

}