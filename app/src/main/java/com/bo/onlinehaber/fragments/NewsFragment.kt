package com.bo.onlinehaber.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import com.bo.onlinehaber.Country
import com.bo.onlinehaber.Language
import com.bo.onlinehaber.R
import com.bo.onlinehaber.adapters.NewsAdapter
import com.bo.onlinehaber.adapters.NewsClickListener
import com.bo.onlinehaber.dialogs.CountryBottomSheetDialog
import com.bo.onlinehaber.dialogs.LanguageBottomSheetDialog
import com.bo.onlinehaber.services.Result
import com.bo.onlinehaber.viewmodels.HomeVM
import com.bo.onlinehaber.viewmodels.MainVM
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.card_view.view.*
import kotlinx.android.synthetic.main.fragment_home.*

class NewsFragment : Fragment(), CountryBottomSheetDialog.BSheetCountryListener,
    LanguageBottomSheetDialog.BSheetLanguageListener, NewsClickListener {

    private lateinit var viewModel: HomeVM
    private lateinit var mainViewModel: MainVM
    private val recyclerNewsAdapter = NewsAdapter(this, arrayListOf())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(HomeVM::class.java)
        mainViewModel = ViewModelProvider(requireActivity())[MainVM::class.java]

        viewModel.refreshData()
        recyclerview_home.adapter = recyclerNewsAdapter
        setListener()
        observeLiveData()

    }

    fun setListener() {

        swipeRefreshLayout.setOnRefreshListener {
            pb_loading.visibility = View.VISIBLE
            linearlayoutNoNews.visibility = View.GONE
            recyclerview_home.visibility = View.GONE
            viewModel.refreshFromInternet()
            swipeRefreshLayout.isRefreshing = false
        }

        constraintLayoutSearch.setOnClickListener {
            val action =
                NewsFragmentDir.actionHomeFragmentToSearchFragment()
            Navigation.findNavController(it).navigate(action)
        }

        constraintLayoutFilter.setOnClickListener {

            if (linearLayoutFilter.visibility == View.GONE) {
                constraintLayoutSearch.visibility = View.GONE
                appbarhome.layoutParams.height = AppBarLayout.LayoutParams.WRAP_CONTENT
                val param = constraintLayoutFilter.layoutParams as LinearLayout.LayoutParams
                param.weight = 10.0f
                constraintLayoutFilter.layoutParams = param
                linearLayoutFilter.visibility = View.VISIBLE
            } else {
                linearLayoutFilter.visibility = View.GONE
                val param = constraintLayoutFilter.layoutParams as LinearLayout.LayoutParams
                param.weight = 5.0f
                constraintLayoutFilter.layoutParams = param
                constraintLayoutSearch.visibility = View.VISIBLE
                appbarhome.layoutParams.height = 150
            }

        }

        constraintLayoutLanguage.setOnClickListener {
            val dialog = LanguageBottomSheetDialog(Language.SupplierLanguage.language, this)
            dialog.show(parentFragmentManager, "dilBottomSheet")
        }

        constraintLayoutCountry.setOnClickListener {
            val dialog = CountryBottomSheetDialog(Country.SupplierCountry.country, this)
            dialog.show(parentFragmentManager, "ulkeBottomSheet")
        }

    }

    fun observeLiveData() {

        viewModel.result.observe(viewLifecycleOwner, Observer { result ->

            when (result) {

                is Result.GetNewsSuccess -> {
                    recyclerview_home.visibility = View.VISIBLE
                    recyclerNewsAdapter.updateNews(result.newses)
                }
                is Result.GetNewsFail -> {
                    if (result.e) {
                        recyclerview_home.visibility = View.GONE
                        linearlayoutNoNews.visibility = View.VISIBLE
                    } else {
                        linearlayoutNoNews.visibility = View.GONE
                    }
                }
                is Result.GetNewsLoading -> {
                    if (result.showLoading) {
                        recyclerview_home.visibility = View.GONE
                        linearlayoutNoNews.visibility = View.GONE
                        pb_loading.visibility = View.VISIBLE
                    } else {
                        pb_loading.visibility = View.GONE
                    }
                }
            }

        })

        mainViewModel.searchLiveData.observe(viewLifecycleOwner, Observer {
            viewModel.searchData(it)
        })


    }

    override fun onClickedCountry(countryText: String) {
        Log.e("country", countryText)
        viewModel.getBottomCountry(countryText)
    }

    override fun onClickedLanguage(languageText: String) {
        Log.e("language", languageText)
        viewModel.getBottomLanguage(languageText)
    }

    override fun onClickListener(view: View) {
        val uuid = view.url_card.text.toString()
        uuid?.let {
            val action =
                NewsFragmentDir.actionHomeFragmentToDetailFragment(
                    it
                )
            Navigation.findNavController(view).navigate(action)
        }
    }
}

class NewsFragmentDir private constructor() {
    private data class ActionHomeFragmentToDetailFragment(
        val url: String
    ) : NavDirections {
        override fun getActionId(): Int =
            R.id.action_homeFragment_to_detailFragment

        override fun getArguments(): Bundle {
            val result = Bundle()
            result.putString("url", this.url)
            return result
        }
    }

    companion object {
        fun actionHomeFragmentToSearchFragment(): NavDirections =
            ActionOnlyNavDirections(R.id.action_homeFragment_to_searchFragment)

        fun actionHomeFragmentToDetailFragment(url: String): NavDirections =
            ActionHomeFragmentToDetailFragment(
                url
            )

        fun actionHomeFragmentToSaveFragment(): NavDirections =
            ActionOnlyNavDirections(R.id.action_homeFragment_to_saveFragment)
    }
}
