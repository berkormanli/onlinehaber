package com.bo.onlinehaber.fragments

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.bo.onlinehaber.R

class SplashActivity : AppCompatActivity() {

    override fun onCreate (savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            //Start main activity and finish splash activity
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            finish()
        },3000)
    }
}