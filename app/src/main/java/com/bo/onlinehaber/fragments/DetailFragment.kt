package com.bo.onlinehaber.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavArgs
import com.bo.onlinehaber.BR
import com.bo.onlinehaber.NewsDBStructure
import com.bo.onlinehaber.R
import com.bo.onlinehaber.databinding.FragmentDetailBinding
import com.bo.onlinehaber.viewmodels.DetailVM
import kotlinx.android.synthetic.main.fragment_detail.*

class DetailFragment : Fragment() {

    private lateinit var viewModel: DetailVM
    private lateinit var dataBinding: FragmentDetailBinding

    var url: String = ""
    var news: NewsDBStructure? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail,container,false)

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        url = arguments?.let { DetailFragmentArgs.fromBundle(it).url }.toString()

        viewModel = ViewModelProviders.of(this).get(DetailVM::class.java)
        url.let { viewModel.roomGetData(it) }
        dataBinding.setVariable(BR.viewmodel,viewModel)

        observeLiveData()
        setListener()

    }

    private fun setListener(){
        imExit.setOnClickListener{
            val count: Int = requireActivity().supportFragmentManager.backStackEntryCount

            if(count == 0){
                super.requireActivity().onBackPressed()
            }else{
                requireActivity().supportFragmentManager.popBackStack()
            }
        }

        imSaves.setOnClickListener {
            if(news?.isSaved!=null){
                if(news?.isSaved == true){
                    //viewModel.updateData(0)
                    viewModel.deleteData()
                    imSaves.setImageResource(R.drawable.ic_saves_off)
                    Toast.makeText(context,"Kaydedilenlerden kaldırıldı", Toast.LENGTH_SHORT).show()
                }

            }else{
                Log.e("isSaved","null")
                viewModel.saveData()
                viewModel.updateData(1)
                imSaves.setImageResource(R.drawable.ic_saves_on)
                Toast.makeText(context,"Haber kaydedildi", Toast.LENGTH_SHORT).show()
            }

        }
    }

    private fun observeLiveData() {

        viewModel.newsLiveData.observe(viewLifecycleOwner, Observer { data ->
            data?.let {data ->
                webView.loadUrl(data)
            }
        })

        viewModel.isSaved.observe(viewLifecycleOwner, Observer {
            if(it){
                imSaves.setImageResource(R.drawable.ic_saves_on)
            }
        })

        viewModel.saveNews.observe(viewLifecycleOwner, Observer {
            news = it
        })
    }
}

class DetailFragmentArgs (
    val url: String
) : NavArgs {
    fun toBundle(): Bundle {
        val result = Bundle()
        result.putString("url", this.url)
        return result
    }

    companion object {
        @JvmStatic
        fun fromBundle(bundle: Bundle): DetailFragmentArgs {
            bundle.setClassLoader(DetailFragmentArgs::class.java.classLoader)
            val __url : String?
            if (bundle.containsKey("url")) {
                __url = bundle.getString("url")
                if (__url == null) {
                    throw IllegalArgumentException("Argument \"url\" is marked as non-null but was passed a null value.")
                }
            } else {
                throw IllegalArgumentException("Required argument \"url\" is missing and does not have an android:defaultValue")
            }
            return DetailFragmentArgs(__url)
        }
    }
}
