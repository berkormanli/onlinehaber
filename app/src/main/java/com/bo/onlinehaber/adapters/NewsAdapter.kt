package com.bo.onlinehaber.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bo.onlinehaber.Articles
import com.bo.onlinehaber.fragments.NewsFragment
import com.bo.onlinehaber.R
import com.bo.onlinehaber.databinding.CardViewBinding

class NewsAdapter(val context: NewsFragment, val items:ArrayList<Articles>) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {
    class ViewHolder(var view: CardViewBinding): RecyclerView.ViewHolder(view.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsAdapter.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = DataBindingUtil.inflate<CardViewBinding>(inflater,
            R.layout.card_view,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){
        holder.view.news = items.get(position)

        holder.view.listener = context
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateNews(newNewsList:List<Articles>){
            items.clear()
            items.addAll(newNewsList)
            notifyDataSetChanged()
    }
}
