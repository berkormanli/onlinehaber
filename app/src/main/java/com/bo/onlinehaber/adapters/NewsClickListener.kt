package com.bo.onlinehaber.adapters

import android.view.View

interface NewsClickListener {
    fun onClickListener(view: View)
}