package com.bo.onlinehaber

import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("top-headlines?country=tr&apiKey=449d1a18c53a42fbab118c8188e876e3")
    fun getNews(): Single<NewsItem>

    @GET("top-headlines")
    fun getTopHeadlines(
        @Query("country") country: String,
        @Query("category") category: String,
        @Query("sources") sources: String,
        @Query("q") search: String,
        @Query("pageSize") pageSize: Int,
        @Query("page") page: Int,
        @Query("apiKey") api: String
    ): Single<NewsItem>

    @GET("everything")
    fun getEverything(
        @Query("q") search: String,
        @Query("qlnTitle") searchTitle: String,
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("language") language: String,
        @Query("sortBy") sortBy: String,
        @Query("apiKey") api: String
    ): Single<NewsItem>

    @GET("top-headlines")
    fun countrySearch(@Query("country") country:String,
                    @Query("apiKey") api_key:String):Single<NewsItem>

    @GET("top-headlines")
    fun languageSearch(@Query("language") language:String,
                     @Query("apiKey") api_key:String):Single<NewsItem>
}