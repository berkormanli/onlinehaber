package com.bo.onlinehaber.services

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager

class SharedPreferencesService {

    companion object{
        private val TIME = "time"
        private var sharedPreferences:SharedPreferences? = null

        @Volatile private var instance : SharedPreferencesService? = null
        private val lock = Any()
        operator fun invoke(context: Context): SharedPreferencesService = instance
            ?: synchronized(lock){
                instance
                    ?: makePrivateSharedPreferences(
                        context
                    ).also {
                        instance = it
                    }
            }

        private fun makePrivateSharedPreferences(context: Context): SharedPreferencesService {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            return SharedPreferencesService()
        }
    }

    fun saveTime(time:Long){
        sharedPreferences?.edit(commit = true){
            putLong(TIME,time)
        }
    }

    fun getTime() = sharedPreferences?.getLong(
        TIME,0)
}