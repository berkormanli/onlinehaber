package com.bo.onlinehaber.services

import com.bo.onlinehaber.NewsDBStructure

sealed class ResultNews {
    class GetNewsSuccess(val newses: List<NewsDBStructure>) : ResultNews()
    class GetNewsFail(val e: Boolean) : ResultNews()
    class GetNewsLoading(val showLoading: Boolean) : ResultNews()
}