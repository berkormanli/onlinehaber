package com.bo.onlinehaber.services

import com.bo.onlinehaber.Articles

sealed class Result {

    class GetNewsSuccess(val newses: List<Articles>) : Result()
    class GetNewsFail(val e: Boolean) : Result()
    class GetNewsLoading(val showLoading: Boolean) : Result()
}