package com.bo.onlinehaber

import android.os.Bundle
import androidx.navigation.NavDirections
import kotlin.Int
import kotlin.String

class SaveFragmentDirections private constructor() {
    private data class ActionSaveFragmentToDetailFragment(
        val url: String
    ) : NavDirections {
        override fun getActionId(): Int = R.id.action_saveFragment_to_detailFragment

        override fun getArguments(): Bundle {
            val result = Bundle()
            result.putString("url", this.url)
            return result
        }
    }

    companion object {
        fun actionSaveFragmentToDetailFragment(url: String): NavDirections =
            ActionSaveFragmentToDetailFragment(url)
    }
}