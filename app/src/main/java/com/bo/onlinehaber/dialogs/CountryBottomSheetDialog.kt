package com.bo.onlinehaber.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bo.onlinehaber.Country
import com.bo.onlinehaber.fragments.NewsFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.bo.onlinehaber.R
import com.bo.onlinehaber.adapters.CountryAdapter
import kotlinx.android.synthetic.main.bottom_sheet_ulke.*

class CountryBottomSheetDialog(var countryList:ArrayList<Country>, var callback: NewsFragment) : BottomSheetDialogFragment() {

    var adapter: CountryAdapter? = null
    var selectedIndex: Int = 0
    var selectedItem: Country? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view: View = inflater.inflate(R.layout.bottom_sheet_ulke, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()
        setListener()
    }

    private fun initUi() {
        countryList.get(selectedIndex).isSelected = false
        adapter = CountryAdapter(countryList)
        listViewCountry.adapter = adapter
    }

    private fun setListener(){
        listViewCountry.setOnItemClickListener { parent, view, position, id ->
            countryList.forEach {
                it.isSelected = false
            }
            countryList.get(selectedIndex).isSelected = false
            countryList.get(position).isSelected = true
            selectedItem = countryList.get(position)
            selectedIndex = position
            adapter?.notifyDataSetChanged()

        }

        buttonCountryClick.setOnClickListener{
            selectedItem?.let { item ->
                callback.onClickedCountry((switch(item.name)))
                this.dismiss()
            }
        }
    }

    private fun switch(countryName: String): String {
        when(countryName){
            "Türkiye" -> return "tr"
            "Almanya" -> return "de"
            "Amerika" -> return "us"
            "İspanya" -> return "es"
            "İngiltere" -> return "gb"
            "Fransa" -> return "fr"
        }
        return ""
    }

    interface BSheetCountryListener {
        fun onClickedCountry(countryText: String)
    }
}