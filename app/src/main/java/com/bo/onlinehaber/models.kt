package com.bo.onlinehaber

import com.bo.onlinehaber.R

class Country(var name:String, var flag: Int,var isSelected:Boolean = false) {

    object SupplierCountry{

        val country = arrayListOf(
            Country("Türkiye", R.drawable.ic_ulke_tr),
            Country("Almanya", R.drawable.ic_ulke_de),
            Country("Amerika", R.drawable.ic_ulke_us),
            Country("İspanya", R.drawable.ic_ulke_es),
            Country("İngiltere", R.drawable.ic_ulke_uk),
            Country("Fransa", R.drawable.ic_ulke_fr)

        )
    }
}

class ExpandableSearch (var title:String, var isSelected:Boolean){

    object SupplierExpandable{

        val header = arrayListOf("Yeni Haberler")

        val releaseDate = arrayListOf(
            ExpandableSearch("Yeni Haberler", false),
            ExpandableSearch("En Çok Okunan", false),
            ExpandableSearch("Manşetler", false)
        )

        var hashExpandable  = hashMapOf(header.first() to  releaseDate)


    }

}

class Language(var name:String, var flag: Int,var isSelected:Boolean = false) {

    object SupplierLanguage{

        val language = arrayListOf(
            Language("Türkçe", R.drawable.ic_ulke_tr),
            Language("Almanca", R.drawable.ic_ulke_de),
            Language("İngilizce", R.drawable.ic_ulke_us),
            Language("İspanyolca", R.drawable.ic_ulke_es),
            Language("İngilizcee", R.drawable.ic_ulke_uk),
            Language("Fransızca", R.drawable.ic_ulke_fr)

        )
    }
}