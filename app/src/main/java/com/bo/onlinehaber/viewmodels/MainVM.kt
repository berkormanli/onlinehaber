package com.bo.onlinehaber.viewmodels

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bo.onlinehaber.Search

class MainVM(application: Application) : BaseVM(application) {

    private val _searchLiveData = MutableLiveData<Search>()

    val searchLiveData: LiveData<Search>
        get() = _searchLiveData

    fun updateSearchValue(search: Search) {

        _searchLiveData.value = search

    }
}