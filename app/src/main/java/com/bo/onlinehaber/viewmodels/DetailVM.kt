package com.bo.onlinehaber.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.bo.onlinehaber.NewsDBStructure
import com.bo.onlinehaber.database.NewsDB
import kotlinx.coroutines.launch

class DetailVM(application: Application) : BaseVM(application) {
    val newsLiveData = MutableLiveData<String>()
    val isSaved = MutableLiveData<Boolean>()
    val saveNews = MutableLiveData<NewsDBStructure>()

    private val dao = NewsDB(getApplication()).newsDao()
    var newsSave: NewsDBStructure? = null


    fun roomGetData(url: String) {
        launch {
            newsLiveData.value = url
            saveNews.value = dao.getSaveNews(url)
            if (saveNews.value == null) {
                Log.e("saveNews", "null")
                val news = dao.getNews(url)
                newsSave = NewsDBStructure(
                    news?.author,
                    news?.title,
                    news?.description,
                    news?.url,
                    news?.urlToImage,
                    news?.publishedAt,
                    news?.content
                )
                isSaved.value = false
            } else {
                Log.e("saveNews", "Haber var -> " + saveNews.value!!.title.toString())
                isSaved.value = true
            }

        }
    }

    fun saveData() {
        launch {
            newsSave?.let {
                dao.insertSaveNews(it)
                Log.e("ASD isSaved", "Kaydedildi")
            }
        }
    }

    fun deleteData() {
        launch {
            dao.deleteSaveNews(saveNews.value?.url!!)
            Log.e("ASD isSaved", "Çıkarıldı")


        }
    }

    fun updateData(isSaved: Int) {
        launch {
            when (isSaved) {

                1 -> newsSave?.let {
                    it.url?.let { it1 -> dao.updateSaved(1, it1) }
                    Log.e("ASD isSaved", "True")
                }


            }
        }

    }
}