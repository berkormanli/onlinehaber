package com.bo.onlinehaber.viewmodels

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.bo.onlinehaber.Articles
import com.bo.onlinehaber.NewsApi
import com.bo.onlinehaber.NewsItem
import com.bo.onlinehaber.Search
import com.bo.onlinehaber.database.NewsDB
import com.bo.onlinehaber.services.Result
import com.bo.onlinehaber.services.SharedPreferencesService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch


class HomeVM(application: Application) : BaseVM(application) {
    val result = MutableLiveData<Result>()
    private var uptadeTime = 5 * 60 * 1000 * 1000 * 1000L

    private val newsApiService = NewsApi()
    private val disposable = CompositeDisposable()
    private val sharedPreferencesHelper = SharedPreferencesService(getApplication())

    fun refreshData() {
        val recordedTime = sharedPreferencesHelper.getTime()

        if (recordedTime != null && recordedTime != 0L && System.nanoTime() - recordedTime < uptadeTime) {
            //Sqlitetan al
            getDataFromSQLite()

        } else {
            getDataFromInternet()
        }
    }

    private fun getDataFromSQLite() {
        result.value = Result.GetNewsLoading(true)

        launch {
            val newsList = NewsDB(getApplication()).newsDao().getAllNews()
            newsShow(newsList)
            Toast.makeText(getApplication(), "Haberleri aldık", Toast.LENGTH_LONG).show()
        }
    }

    private fun getDataFromInternet() {
        result.value = Result.GetNewsLoading(true)

        disposable.add(
            newsApiService.getData()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<NewsItem>() {
                    override fun onSuccess(t: NewsItem) {
                        //Başarılı olursa
                        saveSqlite(t.articles)
                        Toast.makeText(
                            getApplication(),
                            "Haberleri Internetten aldık",
                            Toast.LENGTH_LONG
                        ).show()
                    }

                    override fun onError(e: Throwable) {
                        //Hata alırsak
                        result.value = Result.GetNewsLoading(false)
                        result.value = Result.GetNewsFail(true)
                        e.printStackTrace()
                    }
                })
        )
    }

    fun refreshFromInternet() {
        getDataFromInternet()
    }

    private fun newsShow(newsList: List<Articles>) {
        result.value = Result.GetNewsLoading(false)
        try{
            result.value = Result.GetNewsSuccess(newsList)
        }catch(exception:Exception){
            result.value = Result.GetNewsFail(true)
        }


    }

    private fun saveSqlite(newsList: List<Articles>) {

        launch {
            val dao = NewsDB(getApplication()).newsDao()
            dao.deleteAllNews()
            val uuidListesi = dao.insertAll(*newsList.toTypedArray())
            var i = 0
            while (i < newsList.size) {
                newsList[i].newsID = uuidListesi[i].toInt()
                i++
            }

            newsShow(newsList)
        }

        sharedPreferencesHelper.saveTime(System.nanoTime())
    }

    fun searchData(search: Search) {

        getSearchDataFromInternet(search)
    }

    fun getSearchDataFromInternet(search: Search) {
        result.value = Result.GetNewsLoading(true)

        disposable.add(
            newsApiService.getSearchData(
                search.title,
                search.content,
                search.to,
                search.from,
                search.language,
                search.toSort
            )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<NewsItem>() {
                    override fun onSuccess(t: NewsItem) {
                        //Başarılı
                        saveSqlite(t.articles)
                        Toast.makeText(
                            getApplication(),
                            "Haberleri Internetten aldık search",
                            Toast.LENGTH_LONG
                        ).show()
                    }

                    override fun onError(e: Throwable) {
                        //Hata
                        result.value = Result.GetNewsLoading(false)
                        result.value = Result.GetNewsFail(true)
                        e.printStackTrace()
                    }

                })

        )
    }

    fun getBottomCountry(country: String) {
        result.value = Result.GetNewsLoading(true)

        disposable.add(
            newsApiService.getBottomDataCountry(country)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<NewsItem>() {
                    override fun onSuccess(t: NewsItem) {
                        //Başarılı olursa
                        if (t.totalResults == 0) {
                            result.value = Result.GetNewsLoading(false)
                            result.value = Result.GetNewsFail(true)
                        } else {
                            newsShow(t.articles)
                        }
                    }

                    override fun onError(e: Throwable) {
                        //Hata alırsak
                        result.value = Result.GetNewsLoading(false)
                        result.value = Result.GetNewsFail(true)
                        e.printStackTrace()
                    }
                })
        )
    }

    fun getBottomLanguage(language: String) {
        result.value = Result.GetNewsLoading(true)

        disposable.add(
            newsApiService.getBottomDataLanguage(language)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<NewsItem>() {
                    override fun onSuccess(t: NewsItem) {
                        //Başarılı olursa
                        if (t.totalResults == 0) {
                            result.value = Result.GetNewsLoading(false)
                            result.value = Result.GetNewsFail(true)
                        } else {
                            newsShow(t.articles)
                        }
                    }

                    override fun onError(e: Throwable) {
                        //Hata alırsak
                        result.value = Result.GetNewsLoading(false)
                        result.value = Result.GetNewsFail(true)
                        e.printStackTrace()
                    }
                })
        )
    }
}