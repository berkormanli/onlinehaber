package com.bo.onlinehaber

import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class NewsItem (

    @SerializedName("status") val status : String,
    @SerializedName("totalResults") val totalResults : Int,
    @SerializedName("articles") val articles : List<Articles>
)

data class Articles (

    @SerializedName("author") val author : String,
    @SerializedName("title") val title : String,
    @SerializedName("description") val description : String,
    @SerializedName("url") val url : String,
    @SerializedName("urlToImage") val urlToImage : String,
    @SerializedName("publishedAt") val publishedAt : String,
    @SerializedName("content") val content : String
){@PrimaryKey(autoGenerate = true) var newsID: Int = 0}

data class Source (

    @SerializedName("id") val id : String,
    @SerializedName("name") val name : String
)